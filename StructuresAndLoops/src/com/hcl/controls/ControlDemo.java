package com.hcl.controls;

public class ControlDemo {

	public static void main(String[] args) {
		
		ControlDemo demo =new ControlDemo();//demo is a ref var
        
		boolean x = true;
		
		if(x) {
			System.out.println("if block executed");
		}
		else
		{
			System.err.println("condition fail");
		}
	}

}

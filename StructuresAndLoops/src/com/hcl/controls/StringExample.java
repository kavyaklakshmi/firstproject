package com.hcl.controls;

public class StringExample {

	public static void main(String[] args) {
		String str1="Java";//heap
		String str2=new String("kavya");//heap
		str2="kumar";
		System.out.println(str2);
		System.out.println(str1.length());
		
		System.out.println(str1.charAt(0));
		
		System.out.println(str2.toUpperCase());
		
		String s1 ="A";
		
		String s3 ="A";
		
		System.out.println(s1.equals(s3));//it compares data values
		
		System.out.println(s1==s3);//compares refs,hashcode
		
		String s4 ="A";
		
		String s2 =new String("B");
		
		String s5 =new String("B");
		
		System.out.println(s2.equals(s5));
		System.out.println(s2 ==s5);
		int x=3,y=5;
		System.out.println(x==y);
		

	}

}
